# generators.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""List Vs Generators in Python3.9 -- cProfile Test Result

size of list_comprehensions =  800984
total child count of list_comprehensions =  100000
         300011 function calls in 0.202 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
   100000    0.021    0.000    0.040    0.000 <string>:1(<lambda>)
        1    0.006    0.006    0.202    0.202 <string>:1(<module>)
        1    0.142    0.142    0.189    0.189 generators.py:13(get_max_num_users_using_list)
        1    0.000    0.000    0.196    0.196 generators.py:30(test_list)
        1    0.003    0.003    0.003    0.003 generators.py:31(<listcomp>)
        1    0.003    0.003    0.003    0.003 generators.py:33(<listcomp>)
   100000    0.019    0.000    0.019    0.000 {built-in method __new__ of type object at 0x561c8beeaae0}
        1    0.000    0.000    0.202    0.202 {built-in method builtins.exec}
        2    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.001    0.001    0.001    0.001 {built-in method builtins.sum}
        1    0.000    0.000    0.000    0.000 {built-in method sys.getsizeof}
   100000    0.007    0.000    0.007    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}


size of generator_comprehensions =  112
total child count of generator_comprehensions =  100000
         500011 function calls in 0.200 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
   100000    0.022    0.000    0.041    0.000 <string>:1(<lambda>)
        1    0.000    0.000    0.200    0.200 <string>:1(<module>)
   100001    0.104    0.000    0.145    0.000 generators.py:22(get_max_num_users_using_generators)
        1    0.000    0.000    0.200    0.200 generators.py:37(test_generator)
   100001    0.017    0.000    0.162    0.000 generators.py:38(<genexpr>)
   100001    0.027    0.000    0.190    0.000 generators.py:40(<genexpr>)
   100000    0.019    0.000    0.019    0.000 {built-in method __new__ of type object at 0x561c8beeaae0}
        1    0.000    0.000    0.200    0.200 {built-in method builtins.exec}
        2    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.010    0.010    0.200    0.200 {built-in method builtins.sum}
        1    0.000    0.000    0.000    0.000 {built-in method sys.getsizeof}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
"""

from collections import namedtuple
from collections.abc import Generator
import cProfile
import sys

MAX_NUM = 100000

UserInfo = namedtuple("UserInfo", ["first_name", "last_name", "child_count"])


def get_max_num_users_using_list() -> list[UserInfo]:
    result: list[UserInfo] = []
    for i in range(MAX_NUM):
        result.append(UserInfo(first_name=f"firstname_{i}",
                               last_name=f"last_name_{i}",
                               child_count=1))
    return result


def get_max_num_users_using_generators() -> Generator:
    for i in range(MAX_NUM):
        yield UserInfo(first_name=f"firstname_{i}",
                       last_name=f"last_name_{i}",
                       child_count=1)


if __name__ == "__main__":

    def test_list():
        users_squared_list_comprehensions = [user for user in get_max_num_users_using_list()]
        size = sys.getsizeof(users_squared_list_comprehensions)
        total_child_count = sum([n.child_count for n in users_squared_list_comprehensions])
        print("size of list_comprehensions = ",size)
        print("total child count of list_comprehensions = ", total_child_count)

    def test_generator():
        users_squared_generator_comprehensions = (user for user in  get_max_num_users_using_generators())
        size = sys.getsizeof(users_squared_generator_comprehensions)
        total_child_count = sum((n.child_count for n in users_squared_generator_comprehensions))
        print("size of generator_comprehensions = ",size)
        print("total child count of generator_comprehensions = ", total_child_count)

    cProfile.run("test_list()")
    cProfile.run("test_generator()")
