# deck_cards.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import random

# 花色
SUITS: list[str] = "♠ ♡ ♢ ♣".split()
# 牌
RANKS: list[str] = "2 3 4 5 6 7 8 9 10 J Q K A".split()


def create_deck(shuffle: bool = False) -> list[tuple[str, str]]:
    """洗牌

    Create a new deck of 52 cards
    """

    deck: list[tuple[str, str]] = [(s, r) for r in RANKS for s in SUITS]

    if shuffle:
        random.shuffle(deck)

    return deck


def deal_hands(players: list[str], deck: list[tuple[str, str]]) -> list[tuple[str, list[tuple[str, str]]]]:
    """发牌

    目前发4组牌
    """
    if players is None or len(players) != 4:
        raise ValueError("必须是4个人才能玩牌")

    if any(not isinstance(x, str) for x in players):
        raise TypeError("players中的元素类型为字符串")

    return list(zip(players, (deck[0::4], deck[1::4], deck[2::4], deck[3::4])))


if __name__ == "__main__":
    """开始一个4人局的卡牌游戏
    """
    deck = create_deck(True)
    players: list[str] = "张三 李四 王五 赵六".split()

    play_cards_map = deal_hands(players, deck)

    print(play_cards_map)

    for player, cards in play_cards_map:
        card_str = "\t".join(f"{s}{r}" for (s, r) in cards)
        print(f"{player}:\t{card_str}")
