# sqlite3_samples.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
This module is used to test sqlite3 features
"""

import sqlite3

connection = sqlite3.connect(":memory:")

# Create a table
CREATE_TABLE_HOUSE_DB_SCRIPT = """
CREATE TABLE IF NOT EXISTS houses (
    [id]                        INTEGER PRIMARY KEY AUTOINCREMENT,               
    [housing_estate]            NVARCHAR (100) NOT NULL,
    [address]                   NVARCHAR (200) NOT NULL,
    [build_year]                INTEGER NOT NULL,
    [section_num]               INTEGER NOT NULL,
    [unit_num]                  INTEGER NOT NULL,
    [cell_code]                 NVARCHAR (50) NOT NULL,
    [face_to]                   NVARCHAR(10) NOT NULL,
    [area]                      FLOAT NOT NULL,
    [createdate]                DATETIME DEFAULT (datetime('now', 'localtime'))    
)
"""
connection.execute(CREATE_TABLE_HOUSE_DB_SCRIPT)

# Insert values
connection.executemany(
    """INSERT INTO houses (`housing_estate`,`address`,`build_year`,`section_num`,`unit_num`,`cell_code`,`face_to`,`area`)
    VALUES (?,?,?,?,?,?,?,?)""",
    [
        ("罗阳一村", "罗阳路", 2002, 8, 1, "101", "西南", 100.50),
        ("罗阳一村", "罗阳路", 2002, 8, 2, "202", "西南", 137.00),
        ("罗阳一村", "罗阳路", 2002, 8, 3, "303", "西南", 120.00),
        ("罗阳一村", "罗阳路", 2002, 9, 1, "404", "西南", 100.00),
        ("罗阳一村", "罗阳路", 2002, 9, 3, "503", "西南", 180.00),
        ("罗阳一村", "罗阳路", 2002, 10, 2, "602", "西南", 95.70)
    ]
)

connection.execute(
    """INSERT INTO houses (`housing_estate`,`address`,`build_year`,`section_num`,`unit_num`,`cell_code`,`face_to`,`area`)
    VALUES (?,?,?,?,?,?,?,?)""",
    ("罗阳一村", "罗阳路", 2002, 11, 1, "101", "西南", 110.50)
)

# Print inserted rows
for row in connection.execute("SELECT * FROM houses"):
    print(type(row))
    print(row)
