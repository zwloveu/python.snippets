# request_all_it_ebooks.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import pandas
import re
import sys
import random


class All_itebooks_Spider():
    def _init_(self):
        self.base_url = "http://www.allitebooks.org/page/"
        self.User_Agent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
        self.save_csv_fp = open('IT_ebooks_save.csv', 'w', newline='')
        self.USER_AGENTS = [
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 LBBROWSER",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; 360SE)",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1",
            "Mozilla/5.0 (iPad; U; CPU OS 4_2_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.0b13pre) Gecko/20110307 Firefox/4.0b13pre",
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
            "Mozilla/5.0 (X11; U; Linux x86_64; zh-CN; rv:1.9.2.10) Gecko/20100922 Ubuntu/10.10 (maverick) Firefox/3.6.10"
        ]

    # 1.请求网页
    def get_responce(self, url) -> str:
        res = requests.get(url, self.User_Agent).content.decode('gbk')
        # print(res)
        return res

    # 2.拼接URL翻页
    def get_newUrl(self, page) -> str:
        url = self.base_url+str(page)
        print('正在下载第{}页:'.format(page) + url)
        data = self.get_responce(url)
        return data

    # 3.分析数据，使用bs4匹配想要的信息，进入内部获得书的下载地址
    def ansys_data(self, data) -> list[list]:
        soup = BeautifulSoup(data, 'lxml')
        coment = soup.select('.entry-body')
        info_list = [[], [], [], [], []]
        for one in coment:
            # 3.1匹配书名和链接
            book_url = one.a['href']
            book_name = one.select('.entry-title')[0].get_text()
            # 3.2作者
            auther = one.select('.entry-author')[0].get_text()
            # 3.3简介
            introduction = one.select('.entry-summary')[0].get_text()
            introduction = re.findall(r'\n(.*)\n', introduction)
            # 3.4获取下载链接
            res = requests.get(
                book_url, self.User_Agent).content.decode('utf-8')
            soup_1 = BeautifulSoup(res, 'lxml')
            download_url = soup_1.select('.download-links')
            if len(download_url) == 2:  # 有的书有PDF和EPUB两种格式
                # 优先下载EPUB格式的书，即下载download_url_1
                download_url_1 = download_url[1].a['href']
            else:
                download_url_1 = download_url[0].a['href']
            # 3.5数据汇总
            info_list[0].extend([book_name])
            info_list[1].extend([auther])
            info_list[2].extend([introduction])
            info_list[3].extend([book_url])
            info_list[4].extend([download_url_1])
        return info_list

    # 4.保存数据，保存成cvs
    def save_csv(self, info_list, page):
        saver = pandas.DataFrame({'book_name': info_list[0],
                                  'book_auther': info_list[1],
                                  'book_introduction': info_list[2],
                                  'book_url': info_list[3]
                                  })
        if page == 1:
            saver.to_csv(self.save_csv_fp, index=False,
                         sep=',', mode='a', encoding='utf-8')
        else:
            # header=False不写列名
            saver.to_csv(self.save_csv_fp, header=False,
                         index=False, sep=',', mode='a', encoding='utf-8')

    # 5.下载书籍
    def download_books(self, info_list):
        download_list = info_list[4]
        for url in download_list:
            # 随机浏览器 User-Agent
            headers = {"User-Agent": random.choice(self.USER_AGENTS)}
            file_name = re.findall(r'\d{8}/(.*)', url)
            with open('/home/simon/Downloads' + str(file_name[0]), 'wb') as f:
                print("正在下载", file_name[0])
                response = requests.get(url, stream=True, headers=headers)
                # 获取文件大小
                total_length = response.headers.get('content-length')
                # 如果文件大小不存在，则返回
                if total_length is None:
                    f.write(response.content)
                else:
                    # 下载进度条
                    dl = 0
                    total_length = int(total_length)  # 文件大小
                    fsize = total_length / 1024
                    print("文件大小：{}k，正在下载...".format(fsize))
                    # 每次响应获取 4096 字节
                    for data in response.iter_content(chunk_size=4096):
                        dl += len(data)
                        f.write(data)
                        done = int(100 * dl / total_length)
                        sys.stdout.write("\r[%s%s]" % (
                            '=' * done, ' ' * (100 - done)))  # 打印进度条
                        sys.stdout.write("已下载:{}k".format(dl / 1024))
                        sys.stdout.flush()
                print('\n'+str(file_name[0]) + '下载完成！')

    # 6.运行程序
    def start(self):
        self._init_()
        for page in range(3, 4):
            data = self.get_newUrl(page)
            info_list = self.ansys_data(data)
            self.save_csv(info_list, page)
            self.download_books(info_list)


if __name__ == "__main__":
    All_itebooks_Spider().start()
